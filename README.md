# ocean circulation-altimetry

## Introduction
This is the term-project for the course CE670A Environmental Geodesy, Spring 2023. [TO DO: Add details from the abstract later]


## Programming Languages
1. OCTAVE - Mainly for Spherical Harmonic proecssing using SHBundle and Grace-Bundle
2. Python - For rest of the analysis and visualizations


## Getting Started
```
cd existing_repo
git remote add origin https://gitlab.com/abhimhamane18/ocean-circulation-altimetry.git
git branch -M main
git push -uf origin main
```

## Collaborating
- Create issues to flag bugs in the code or documentation
- Create a pull request to merge your code into main branch


## Important Python Modules
- `numpy`
- `cartopy`
- `matplotlib`
-  `os`
- `ftplib`
- `xarray` or `netCDF4`
- `pandas`

## Authors and acknowledgment
- Abhishek Mhamane (MS-R Geoinformatics, IIT Kanpur)
- Shubhi Kant (BT-MT Geoinformatics, IIT Kanpur)
- Ayush Gupta (BT-MT Geoinformatics, IIT Kanpur)

.
