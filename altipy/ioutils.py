"""This module contais functions related to Input/Output
"""

# baisc imports
import os
import pandas as pd
import netCDF4 as nc
import xarray as xr
from tqdm import tqdm
import numpy as np


def set_data_dir(data_dir_path: str):
    """ Set the path to altimetry data directory on your local system.

    Inputs
    ------
    data_dir_path: Full path to the data dir
    """
    return f"{data_dir_path}"



def get_required_param(data_dir_path: str, param: pd.DataFrame):
    """ Returns the path to csv file containing the information regarding cycle,
    pass, lat, lon and param values
    """

    pass



def pre_process_OpenADB(data_folder_path):
    """ Pre Process data downloaded from the OpenADB-TUM Portal

    Inputs
    -------
    data_folder_path: absolute path in terms of raw string


    Outputs
    -------
    A pandas DataFrame which is easy to work with.
    Columns = [cycle no. ,pass no., lat [deg], lon [deg], ssh [m]]

    """
    mission_df = pd.DataFrame(columns=['cycle', 'pass','lat', 'lon', 'ssh'])
    
    print(f"Processing the OpenADB Altimetry data files at {data_folder_path}")

    cycles = os.listdir(data_folder_path)
    pbar = tqdm(cycles, total=len(cycles))

    for c in cycles:
        files = os.listdir(data_folder_path + "\\" + c)
        pbar.set_description(f"Processing Cycle No: {c}")
        for f in files:
            ds = nc.Dataset(data_folder_path + "\\" + c + '\\' + f, 'r')
            
            lat = ds['glat.00'][:]
            lon = ds['glon.00'][:]
            ssh = ds['ssh.40'][:]
            cycle = np.int32(ds.cycle) * np.ones(len(lat))
            pass_number = np.int32(ds.pass_number) * np.ones(len(lat))

            new_df = pd.DataFrame(np.array([cycle, pass_number, lat, lon, ssh]).T, columns=mission_df.columns)
            mission_df = pd.concat([mission_df, new_df], ignore_index=True)

        pbar.update(1)
        
    pbar.close()

    return mission_df


def read_geoid_file(file_path, column_header: list):
    """ This it to read the comma seperated txt file which was generated after
    processing the Spherical Harmonic Coeffients in OCTAVE using SHbundle

    Inputs
    ------
    file_path - absolute file path
    column_header - list of column header

    Outputs
    -------
    dataframe with appropriate header
    """

    geoid = pd.read_csv(file_path, sep=',', header=None)
    geoid.columns = column_header

    return geoid


def anisodiff(image,niter=1,kappa=0.16,gamma=0.25,step=(1,1)):
    """ Anisotropic Diffusion Filter
    
    """

    # ...you could always diffuse each color channel independently if you
    # really want
    img = image.copy()
    mask = np.isnan(image)
    img[mask==True] = 0
    if img.ndim == 3:
        warnings.warn("Only grayscale images allowed, converting to 2D matrix")
        img = img.mean(2)

    # initialize output array
    img = img.astype('float32')
    imgout = img.copy()

    # initialize some internal variables
    deltaS = np.zeros_like(imgout)
    deltaE = deltaS.copy()
    NS = deltaS.copy()
    EW = deltaS.copy()
    gS = np.ones_like(imgout)
    gE = gS.copy()

    for ii in np.arange(1,niter):
        # calculate the diffs i.e. gradients
        deltaS[:-1,: ] = np.diff(imgout,axis=0)
        deltaE[: ,:-1] = np.diff(imgout,axis=1)
        deltaSf=deltaS;
        deltaEf=deltaE;

        # conduction gradients (only need to compute one per dim!)
        DS = 1./(1.+(deltaSf/kappa)**2.)/step[0]
        DE = 1./(1.+(deltaEf/kappa)**2.)/step[1]

        # update matrices
        E = DE*deltaE
        S = DS*deltaS

        # subtract a copy that has been shifted 'North/West' by one pixel
        NS[:] = S
        EW[:] = E
        NS[1:,:] -= S[:-1,:]
        EW[:,1:] -= E[:,:-1]

        # update the image
        imgout += gamma*(NS+EW)
        print('Min: {}, Max: {}, Mean: {}'.format(imgout.min(), imgout.max(), imgout.mean()))
        print('-----------------------------------------')
        
    imgout[mask] = np.nan
    return imgout