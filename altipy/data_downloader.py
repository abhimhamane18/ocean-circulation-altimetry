""" This module has functions required to download altimetry data
"""

import os
import ftplib
from tqdm import tqdm

def download_jason_gdr(passes: list, cycles: list, download_folder: str):
    """ Downloads the Jason-2 GDR from the appropriate ftp server

    Inputs
    ------
    passes: list of passes required
    cycles: list of cycles needed to be downloaded
    download_folder: absolute path to the download folder

    Outputs
    -------
    Downloads the required files into the given download folder
    """
    # ftp server link
    FTP_HOST = "ftp-oceans.ncei.noaa.gov"
    ftp = ftplib.FTP(FTP_HOST, 'anonymous', '')
    ftp.encoding = "utf-8"

    print(ftp.getwelcome())

    ftp.cwd('/pub/data.nodc/jason2/gdr_d/gdr')
    print(f"FTP Data dir: {ftp.pwd()} \n")
    #ftp.retrlines('LIST')
    #291
    # store files in the download folder of users choice
    os.chdir(download_folder)
    print(f"Downloading the files into {os.getcwd()} \n")

    # initiate a progress bar
    cpbar = tqdm(cycles, total=len(cycles))
    for i in cycles:
        files = ftp.nlst('cycle' + str(i))
        for p in passes:
            cpbar.set_description(f'Downloading Cycle No. {i} and Pass No. {p}')
            for f in files:
                if f[25:28] == p:
                    with open(f[9:] + '.nc', "wb") as file:
                        ftp.retrbinary(f"RETR {f}", file.write)
                    break 
            # update the progress bar with every pass downloaded
            cpbar.update(1/len(passes))
    
    ftp.quit()

def download_saral_gdr(passes: list, cycles: list, download_folder: str, login_cred: dict):
    """Downloads the Saral GDR from the appropriate ftp server

    Inputs
    ------
    passes: list of passes required
    cycles: list of cycles needed to be downloaded
    download_folder: absolute path to the download folder
    login_cred: Dictionary with user_name and password

    Outputs
    -------
    Saves the files into given download folder
    """
    # ftp server link
    FTP_PORT = 21
    FTP_HOST = "ftp-access.aviso.altimetry.fr"
    ftp = ftplib.FTP(FTP_HOST)
    ftp.login(login_cred['username'], login_cred['password'])
    ftp.encoding = "utf-8"

    # welcome message
    print(f"{ftp.getwelcome()}")

    ftp.cwd('/geophysical-data-record/saral/gdr_f')

    print(f"FTP Data dir: {ftp.pwd()}")

    os.chdir(download_folder)
    print(f"Downloading the files into {os.getcwd()} \n")

    # navigating the ftp server to get required files
    cpbar = tqdm(cycles, total=len(cycles))
    for i in cycles:
        files = ftp.nlst('cycle_' + str(i).zfill(3))
        for p in passes:
            cpbar.set_description(f'Downloading Cycle No. {i} and Pass No. {p}')
            for f in files:
                if f[26:30] == p:
                    with open(f[10:] + '.nc', "wb") as file:
                        ftp.retrbinary(f"RETR {f}", file.write)
                    break
            # update the progress bar with every pass downloaded
            cpbar.update(1/len(passes)) 
    ftp.quit()
