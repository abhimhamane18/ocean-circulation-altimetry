
import cartopy
import cartopy.crs as ccrs
import matplotlib.pyplot as plt 
import xarray as xr
from shapely.geometry.polygon import Polygon


"""
This module contains functions required for visualisation of altimetry data
"""

def viz_tracks():
    
    pass

def viz_altimetry_param():
    pass

def plot_params():
    pass

