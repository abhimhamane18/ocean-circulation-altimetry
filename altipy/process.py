"""
This script has all the functions required for processing of altimetry data
"""
import numpy as np
import pandas as pd
from scipy.interpolate import griddata


def create_grid(lon, lat, values, extent, interval):
    """ create_grid generates a regular grid for given input params

    Inputs
    -------
    lon: longitude [deg]
    lat: latitude [deg]
    values: corresponding parameter array/list
    extent: [lon_min, lon_max, lat_min, lat_max]
    interval: grid spacing [deg]

    Output
    ------
    grid: values of param gridded onto the grid
    X,Y: meshgrid for given
    """
    x = np.arange(extent[0], extent[1], interval)
    y = np.arange(extent[2], extent[3], interval)
    
    points = np.array([lon, lat]).T
    
    X, Y = np.meshgrid(x, y)
    
    grid = griddata(points, values, (X,Y), method='linear')
    
    return grid, X, Y


def TP2WGS84(data_df, param):
    """ Converts the given altimetry mss in T/P ellipsoid to WGS84 ellipsoid

    Inputs
    ------
    mss: the mean sea surface in array or pd.DataFrame column [m]

    Output
    ------
    mss converted to WGS84 elliposid

    Notes
    -----
    Using an approximate method with reference to 

        @article{Andersen1996,
        author = {Andersen, O. B. and Knudsen, P.},
        doi = {10.1016/S0079-1946(97)00058-X},
        journal = {Physics and Chemistry of the Earth},
        title = {{Altimetric gravity field from the full ERS-1 geodetic mission}},
        year = {1996}
        }
    """
    return data_df[param] -= 0.7


def compute_mss(data_df):
    """ Computes Mean Sea Surface heights along each track

    Inputs
    ------
    data_df: pandas DataFrame 
    """
    mission_df = pd.DataFrame(columns=['pass', 'lat', 'lon', 'mssh'])

    data_df = data_df.groupby('pass')

    for name, group in data_df:
        cycle_max = group.groupby('cycle').size().idxmax()
        # fixing the trac lats and lons
        lat_fixed = group.loc[group['cycle'] == cycle_max, 'lat']
        lon_fixed = group.loc[group['cycle'] == cycle_max, 'lon']
        mssh = np.array(group.loc[group['cycle'] == cycle_max, 'ssh'])
        #print(mssh.shape)
        grouped = group.groupby('cycle')
        for cyc, frame in grouped:
            if cyc == cycle_max:
                continue
            if len(frame['ssh']) < 4:
                continue

            zi = griddata((frame['lon'], frame['lat']), frame['ssh'] , (lon_fixed, lat_fixed), method='linear')
            #print(zi.shape)
            mssh = np.vstack((mssh, zi))
            #print(mssh.shape)
        mssh = np.nanmean(mssh, axis=0)
        pas = name*np.ones(len(lat_fixed))

        df_arr = np.asarray([pas, lat_fixed, lon_fixed, mssh]).T
        new_df = pd.DataFrame(df_arr, columns=mission_df.columns)

        # concatenate the new DataFrame with the original DataFrame
        mission_df = pd.concat([mission_df, new_df], ignore_index=True)

    return mission_df


def identify_ascending_tracks(data_df):

    pass


def identify_descending_tracks(data_df):


    pass


def haversine(lat1, lon1, lat2, lon2):
    """ Computes the Haver Sine b/w given two points.

    Inputs
    ------
    lat1, lat2 - Latitude of 2 points [deg]
    lon1, lon2 - Longitude of 2 points [deg]

    Outputs
    -------
    Haver sine distance [kms]

    Notes
    ------
    Formula reference: [TO DO]
    """
    R = 6371  # radius of the earth in km
    dlat = np.radians(lat2 - lat1)
    dlon = np.radians(lon2 - lon1)
    a = np.sin(dlat/2)**2 + np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) * np.sin(dlon/2)**2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
    d = R * c
    return d

def find_crossover_points(lat1, lon1, lat2, lon2, threshold):
    """ Identifies Cross-Over points b/w two tracks, based on the haversine distance

    Inputs
    ------
    lat1, lat2 - Latitude of 2 points to be checked for cross-over [deg]
    lon1, lon2 - Longitude of 2 points to be checked for cross-over [deg]
    threshold - Distance in km below which the two points are considered to cross-over
    
    Outputs
    -------
    List of cross-over points indices
    """

    print(f"Threshold considered for cross-over {threshold} [km]")
    crossover_points = set()
    for i in range(len(lat1)-1):
        for j in range(i+1 , len(lat2)-1):
            d = haversine(lat1[i], lon1[i], lat2[j], lon2[j])
            
            if d <= threshold:
                # print(lat1[i], lon1[i], lat2[j], lon2[j])
                crossover_points.add(j)
    return crossover_points


