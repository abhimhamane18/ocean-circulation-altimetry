# Introduction

## Points on Altimetry, Geodesy and Geoids

[Abdalla2021] - Discusses the contributions of Altimetry and the way ahead
- 

[LeTraon2013] - Discusses the revolutions brought by Altimetry and Argo floats in oceanography



## Studies around the world
[DucetN.2000] - Mapped the ocean circulation at global scale using TOPEX

[Eales2016] - studied the climatology of surface geostrophic circulation using satellite altimetry and GOCE gravity data

[Igo2018] - explored the surface geostrophic currents using satellite graivty and altimetry data for the Mediterranean sea

[Volkov2012] - studied the GOCE and GRACE derived MDT for computing Antarctic Circumpolar Current

[Pasuya2015] - analysed the sea surface currents in the Gulf of Thailand using satellite altimetry and GPS trackced drifting buoys.

[Observatory2014] - studied the errors in MDT and its impact on the Geostrophic Current Estimates for the seas around mainland china


## Points on Ocean Circulation

[Shankar2002] - analysed the monsoon currents of Northern Indian Ocean 


## Present study area

The study area choosen [65E, 75E, -10S, 5N]
