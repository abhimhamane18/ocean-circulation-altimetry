import os
import ftplib

# Downloading Jason 2 IGDR products

FTP_HOST = "ftp-oceans.ncei.noaa.gov"
ftp = ftplib.FTP(FTP_HOST, 'anonymous', '')
ftp.encoding = "utf-8"

ftp.cwd('/pub/data.nodc/jason2/gdr_d/gdr')
#ftp.retrlines('LIST')
#291

passes = ['003', '016', '029', '042', '066', '079', '092', '105', '118', '131', '155', '168', '181', '194', '207', '220', '231', '244']
for i in range (178, 291):
    files = ftp.nlst('cycle' + str(i))
    for p in passes:
        for f in files:
            if f[25:28] == p:
                with open(f[9:] + '.nc', "wb") as file:
                    ftp.retrbinary(f"RETR {f}", file.write)
                break
    print(f'cycle {i}: completed')
